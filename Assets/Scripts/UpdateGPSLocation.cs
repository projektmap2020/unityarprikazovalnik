﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateGPSLocation : MonoBehaviour
{
    public Text GPSCoordinates;
    public Text ShortestDistance;
    public Text ConditionText;
    public GameObject dataFromServer;

    public GameObject crossSign;
    public GameObject tickSign;
    public GameObject stopSign;

    private PrepareDataFromServer prepareDataFromServer;
    private List<RoadSignData> roadSignData;
    private List<RoadConditionData> roadConditionData;

    void Start()
    {
        prepareDataFromServer = dataFromServer.GetComponent<PrepareDataFromServer>();
        roadConditionData = prepareDataFromServer.GetRoadCondition();
        roadSignData = prepareDataFromServer.GetRoadSigns();
        tickSign.SetActive(false);
        crossSign.SetActive(false);
        stopSign.SetActive(false);
    }

    void Update()
    {
        double lat = GPS.Instance.latitude;
        double lon = GPS.Instance.longitude;

        if (lat > 0.0)
        {
            prepareDataFromServer.UpdateShortestDistance(lat, lon);

            if (prepareDataFromServer.GetShortestCondition() < 50)
            {
                if (prepareDataFromServer.GetCondition().condition == 0)
                {
                    ConditionText.text = "Dobro cestisce";
                    ConditionText.color = Color.green;
                    tickSign.SetActive(true);
                    crossSign.SetActive(false);
                } else if (prepareDataFromServer.GetCondition().condition == 1) {
                    ConditionText.text = "Slabo cestisce";
                    ConditionText.color = Color.red;
                    tickSign.SetActive(false);
                    crossSign.SetActive(true);
                }
            } else if (prepareDataFromServer.GetShortestCondition() > 1500) {
                ConditionText.text = "Ni podatka o stanju";
                ConditionText.color = Color.white;
                tickSign.SetActive(false);
                crossSign.SetActive(false);
            }

            if (prepareDataFromServer.GetShortestSign() < 100)
            {
                stopSign.SetActive(true);
            } else
            {
                stopSign.SetActive(false);
            }

            ShortestDistance.text = "Naslednji stop zank:\n" + prepareDataFromServer.GetShortestSign().ToString() + "m";
            ShortestDistance.color = Color.red;

            GPSCoordinates.text = "Lati: " + lat.ToString() + "\n" +
                                  "Long: " + lon.ToString();
        }
        else {
            GPSCoordinates.text = "GPS not returning\nany values";
        }
        
    }

    double degreesToRadians(double degrees)
    {
        return degrees * Math.PI / 180f;
    }

    float DistanceBetweenPointsInMetres(double lat1, double lon1, double lat2, double lon2)
    {
        float earthRadiusKm = 6375f;

        var dLat = degreesToRadians(lat2 - lat1);
        var dLon = degreesToRadians(lon2 - lon1);

        lat1 = degreesToRadians(lat1);
        lat2 = degreesToRadians(lat2);

        var a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                Math.Sin(dLon / 2) * Math.Sin(dLon / 2) * Math.Cos(lat1) * Math.Cos(lat2);
        var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
        return (float)(earthRadiusKm * c) * 1000f;
    }
}
