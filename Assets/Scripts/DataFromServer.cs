﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

[System.Serializable]
public class RoadConditions
{
    public List<RoadConditionData> roadData = new List<RoadConditionData>();
}

[System.Serializable]
public class RoadConditionData
{
    public int condition;
    public double longitude;
    public double latitude;
}



[System.Serializable]
public class RoadSigns
{
    public List<RoadSignData> roadSigns = new List<RoadSignData>();
}



[System.Serializable]
public class RoadSignData
{
    public string signType;
    public double longitude;
    public double latitude;
}

public class DataFromServer : MonoBehaviour
{
    protected string roadConditionDataURL = "http://83.212.127.94:3000/api/rc/";
    protected string roadSignDataURL = "http://83.212.127.94:3000/api/ts/";
    public List<RoadSignData> trafficSignsData = new List<RoadSignData>();
    public List<RoadConditionData> conditionData = new List<RoadConditionData>();

    public void GetSignDataFromServer()
    {
        _ = StartCoroutine(GetTrafficSignData());
    }
    
    IEnumerator GetTrafficSignData()
    {
        UnityWebRequest serverRequest = UnityWebRequest.Get(roadSignDataURL);
        yield return serverRequest.SendWebRequest();

        if (serverRequest.isNetworkError || serverRequest.isHttpError)
        {
            Debug.Log("ERROR" + serverRequest.error);
        }
        else
        {
            ProcessJSONTrafficSigns(serverRequest.downloadHandler.text);
        }
    }


    private void ProcessJSONTrafficSigns(string data)
    {
        data = "{\"roadSigns\":" + data + "}";
        RoadSigns roadSigns = JsonUtility.FromJson<RoadSigns>(data);

        foreach (RoadSignData signs in roadSigns.roadSigns)
        {
            trafficSignsData.Add(signs);
            string debugString = signs.signType + " " + signs.latitude.ToString() + " " + signs.longitude.ToString();
            Debug.Log(debugString);
        }

    }

    public void GetRoadDataFromServer()
    {
        _ = StartCoroutine(GetRoadData());
    }


    IEnumerator GetRoadData()
    {
        UnityWebRequest serverRequest = UnityWebRequest.Get(roadConditionDataURL);
        yield return serverRequest.SendWebRequest();


        if (serverRequest.isNetworkError || serverRequest.isHttpError)
        {
            Debug.Log("ERROR" + serverRequest.error);
        }
        else
        {
            ProcessJSONRoadConditions(serverRequest.downloadHandler.text);
        }
    }


    private void ProcessJSONRoadConditions(string data)
    {
        data = "{\"roadData\":" + data + "}";
        RoadConditions conditions = JsonUtility.FromJson<RoadConditions>(data);

        foreach (RoadConditionData condition in conditions.roadData)
        {
            conditionData.Add(condition);
            string debugString = condition.condition.ToString() + " " + condition.latitude.ToString() + " " + condition.longitude.ToString();
            Debug.Log(debugString);
        }

    }
}