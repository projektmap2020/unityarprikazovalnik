﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrepareDataFromServer : MonoBehaviour
{
    DataFromServer dataFromDatabase;

    private float shortestDistanceCondition;
    private float shortestDistanceSign;
    private RoadConditionData condition;

    void Start()
    {
        dataFromDatabase = gameObject.AddComponent<DataFromServer>();
        dataFromDatabase.GetSignDataFromServer();
        dataFromDatabase.GetRoadDataFromServer();
        shortestDistanceCondition = 150000f;
        shortestDistanceSign = 150000f;
    }

    public void UpdateShortestDistance(double lat, double lon)
    {
        for (int i = 0; i < GetRoadSigns().Count; i++)
        {
            if (shortestDistanceSign > DistanceBetweenPointsInMetres(lat, lon, GetRoadSigns()[i].latitude, GetRoadSigns()[i].longitude))
            {
                shortestDistanceSign = DistanceBetweenPointsInMetres(lat, lon, GetRoadSigns()[i].latitude, GetRoadSigns()[i].longitude);
            }
        }
        for (int i = 0; i < GetRoadCondition().Count; i++)
        {
            if (shortestDistanceCondition > DistanceBetweenPointsInMetres(lat, lon, GetRoadCondition()[i].latitude, GetRoadCondition()[i].longitude))
            {
                shortestDistanceCondition = DistanceBetweenPointsInMetres(lat, lon, GetRoadCondition()[i].latitude, GetRoadCondition()[i].longitude);
                condition = GetRoadCondition()[i];
            }
        }
    }

    private double degreesToRadians(double degrees)
    {
        return degrees * Math.PI / 180f;
    }

    public float DistanceBetweenPointsInMetres(double lat1, double lon1, double lat2, double lon2)
    {
        float earthRadiusKm = 6375f;

        var dLat = degreesToRadians(lat2 - lat1);
        var dLon = degreesToRadians(lon2 - lon1);

        lat1 = degreesToRadians(lat1);
        lat2 = degreesToRadians(lat2);

        var a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                Math.Sin(dLon / 2) * Math.Sin(dLon / 2) * Math.Cos(lat1) * Math.Cos(lat2);
        var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
        return (float)(earthRadiusKm * c) * 1000f;
    }

    public List<RoadSignData> GetRoadSigns()
    {
        return dataFromDatabase.trafficSignsData;
    }

    public List<RoadConditionData> GetRoadCondition()
    {
        return dataFromDatabase.conditionData;
    }

    public float GetShortestCondition()
    {
        return shortestDistanceCondition;
    }

    public float GetShortestSign()
    {
        return shortestDistanceSign;
    }

    public RoadConditionData GetCondition()
    {
        return condition;
    }
}
